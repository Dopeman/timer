//
//  TIGPSController.swift
//  Timer
//
//  Created by David on 11/7/16.
//  Copyright © 2016 David. All rights reserved.
//

import UIKit
import MapKit

class TIGPSController: UIViewController,CLLocationManagerDelegate {
    
    @IBOutlet weak var startTimerButton: TIRoundButton!
    @IBOutlet weak var contolPanelView: UIView!
    @IBOutlet weak var mapView: MKMapView!
    
    var initialRegion:MKCoordinateRegion!
    var locationManager:CLLocationManager! = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.commonInit();
    }
    
    func commonInit() {
        // Setup transparent gradient
        let gradient = CAGradientLayer()
        gradient.frame = contolPanelView.bounds;
        
        gradient.colors = [UIColor.init(white: 0, alpha: 0.2).cgColor,UIColor.init(white: 0, alpha: 0.65).cgColor]
        gradient.locations = [0.0,1.0]
        gradient.startPoint = CGPoint(x:0.0, y:0.0)
        gradient.endPoint = CGPoint(x:0.5, y:1.0)
        contolPanelView.layer.insertSublayer(gradient, at: 0)
        
        startTimerButton.borderColor = UIColor(red:182.0/255.0, green:145.0/255.0, blue:14.0/255.0, alpha:1.0)
        
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        locationManager.distanceFilter = kCLDistanceFilterNone;
        locationManager.requestWhenInUseAuthorization()
        
        initialRegion = mapView.region
    }
    
    @IBAction func startTrackingUserLocation(_ sender: Any) {
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            mapView.showsUserLocation = true
        }
      
        locationManager.startUpdatingLocation()
        locationManager.startUpdatingHeading()
    }
    
    @IBAction func stopTrackingUserLocation(_ sender: Any) {
        locationManager.stopUpdatingLocation()
        locationManager.stopUpdatingHeading()
        mapView.setRegion(initialRegion, animated: true)
    }
    
     // MARK: CLLocationManagerDelegate data source
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        let region = MKCoordinateRegionMakeWithDistance(locValue, 1000, 1000);
        
        mapView.setRegion(region, animated: true)
    }
}


