//
//  TITabBarController.swift
//  Timer
//
//  Created by David on 11/5/16.
//  Copyright © 2016 David. All rights reserved.
//

import UIKit


class TITabBarController: UITabBarController, UIPickerViewDataSource, UIPickerViewDelegate {
    
    var modeTitleLabel:UILabel!
    var modePickerView:UIView!
    var modes: [String] = [String]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabBar.isHidden = true
        
        let selectModeButton = UIButton(type:UIButtonType.system)
        selectModeButton.setTitleColor(UIColor.white, for: UIControlState.normal)
        selectModeButton.setTitleColor(UIColor.white, for: UIControlState.selected)
        selectModeButton.setTitle("Mode", for: UIControlState.normal)
        selectModeButton.tintColor = UIColor.clear
        selectModeButton.clipsToBounds = true
      
        selectModeButton.contentHorizontalAlignment = UIControlContentHorizontalAlignment.left
        selectModeButton.setImage(UIImage(named:"dropdown_icon_normal")?.withRenderingMode(.alwaysOriginal), for: UIControlState.normal)
        selectModeButton.setImage(UIImage(named:"dropdown_icon_selected")?.withRenderingMode(.alwaysOriginal), for: UIControlState.selected)
        selectModeButton.imageEdgeInsets = UIEdgeInsets(top: 0.0, left:55.0, bottom: 0.0, right:0.0)
        selectModeButton.addTarget(self, action:#selector(selectModeButtonDidTap(_:)), for: .touchUpInside)
        self.view.addSubview(selectModeButton)
        
        modeTitleLabel = UILabel()
        modeTitleLabel.textAlignment = NSTextAlignment.left
        modeTitleLabel.textColor = UIColor.init(white: 1.0, alpha: 0.5)
        self.view.addSubview(modeTitleLabel)
        setTitleForMode(mode:"Timer")
        
        // Layout views
        selectModeButton.translatesAutoresizingMaskIntoConstraints = false;
        modeTitleLabel.translatesAutoresizingMaskIntoConstraints = false;
        
        var horizontalConstraint:NSLayoutConstraint = NSLayoutConstraint(item: selectModeButton, attribute: NSLayoutAttribute.right, relatedBy: NSLayoutRelation.equal, toItem: self.view, attribute: NSLayoutAttribute.centerX, multiplier: 1.0, constant: 15.0);
        var verticalConstraint:NSLayoutConstraint = NSLayoutConstraint(item: selectModeButton, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: self.view, attribute: NSLayoutAttribute.top, multiplier: 1.0, constant: 25.0);
        let widthConstraint:NSLayoutConstraint = NSLayoutConstraint(item: selectModeButton, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute:NSLayoutAttribute.notAnAttribute, multiplier: 1.0, constant: 100.0);
        self.view.addConstraint(widthConstraint);
        self.view.addConstraint(horizontalConstraint);
        self.view.addConstraint(verticalConstraint);
        
        horizontalConstraint = NSLayoutConstraint(item: modeTitleLabel, attribute: NSLayoutAttribute.left, relatedBy: NSLayoutRelation.equal, toItem: selectModeButton, attribute: NSLayoutAttribute.right, multiplier: 1.0, constant: -15.0);
        verticalConstraint = NSLayoutConstraint(item: modeTitleLabel, attribute: NSLayoutAttribute.centerY, relatedBy: NSLayoutRelation.equal, toItem: selectModeButton, attribute: NSLayoutAttribute.centerY, multiplier: 1.0, constant: 0.0);
        self.view.addConstraint(horizontalConstraint);
        self.view.addConstraint(verticalConstraint);
    }
    
    func setTitleForMode(mode:String) {
        let separatorTextAttributes = [NSForegroundColorAttributeName: UIColor.white];
        let modeTextAttributes = [NSForegroundColorAttributeName: UIColor.init(white: 1.0, alpha: 0.5)];
        let separatorText = NSMutableAttributedString(string: ": ", attributes: separatorTextAttributes)
        let modeText = NSMutableAttributedString(string: mode, attributes: modeTextAttributes)
        
        let attributedTitle = NSMutableAttributedString()
        
        attributedTitle.append(separatorText)
        attributedTitle.append(modeText)
        
        modeTitleLabel.attributedText = attributedTitle
        
        if let index = modes.index(of: mode) {
            self.selectedIndex = index
        }
    }


     func selectModeButtonDidTap(_ sender : UIButton) {
        if(sender.isSelected) {
            sender.isSelected = false;
        }
        else {
             sender.isSelected = true;
        }
        showModePickerView(flag:sender.isSelected)
    }
    
    func showModePickerView(flag:Bool) {
        var transform:CGAffineTransform!
        if(flag) {
            if(modePickerView == nil) {
                modes = ["Timer", "Stopwatch", "GPS"]
                let modePicker = UIPickerView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 150))
                modePicker.delegate = self
                modePicker.dataSource = self
                
                modePickerView = UIView(frame: CGRect(x: 0, y: 50, width: self.view.frame.width, height: 150))
                modePickerView.backgroundColor = UIColor(white:0.0,alpha:0.8)
                modePickerView.addSubview(modePicker)
                view.addSubview(modePickerView)
                modePickerView.transform = CGAffineTransform(scaleX:0, y:0);
            }
            transform = CGAffineTransform(scaleX:1.0, y:1.0)
        } else {
            transform = CGAffineTransform(scaleX:0.001, y:0.001)
        }
       
        UIView.animate(withDuration: 0.3, animations: {
             self.modePickerView.transform = transform
        })
    }
    
    // MARK: UIPickerView data source
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return modes.count
    }
 
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
         setTitleForMode(mode:modes[row])
    }
    
    public func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString?
    {
        let attributes = [ NSForegroundColorAttributeName: UIColor.white ]
        return NSAttributedString(string: modes[row], attributes: attributes)
    }
}


