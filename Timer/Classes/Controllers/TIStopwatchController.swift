//
//  TIStopwatchController.swift
//  Timer
//
//  Created by David on 11/5/16.
//  Copyright © 2016 David. All rights reserved.
//

import UIKit

class TIStopwatchController: UIViewController {
     @IBOutlet weak var contolPanelView: UIView!
     @IBOutlet weak var startTimerButton: TIRoundButton!
     @IBOutlet weak var timeLabel: UILabel!
     @IBOutlet weak var timeLabelTopConstraint: NSLayoutConstraint!
    
     var timer:Timer!
     var timeInterval:TimeInterval! = 0
    
     override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.commonInit();
     }
    
    func commonInit() {
        // Setup transparent gradient
        let gradient = CAGradientLayer()
        gradient.frame = contolPanelView.bounds;
        
        gradient.colors = [UIColor.init(white: 0, alpha: 0.2).cgColor,UIColor.init(white: 0, alpha: 0.65).cgColor]
        gradient.locations = [0.0,1.0]
        gradient.startPoint = CGPoint(x:0.0, y:0.0)
        gradient.endPoint = CGPoint(x:0.5, y:1.0)
        contolPanelView.layer.insertSublayer(gradient, at: 0)
        
        startTimerButton.borderColor = UIColor(red:182.0/255.0, green:145.0/255.0, blue:14.0/255.0, alpha:1.0)
        
        timeLabelTopConstraint.constant = (view.frame.height - contolPanelView.frame.origin.y) / 2 ;
    }
    
    @IBAction func startButtonDidTap(_ sender: UIButton) {
        if(timer != nil && timer.isValid) {
            timer.invalidate()
        } else {
            timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(TITimerViewController.countDownTimer), userInfo: nil, repeats: true) 
        }
    }
    
    @IBAction func resetButtonDidTap(_ sender: UIButton) {
        timer.invalidate()
        timeLabel.text = "00m:00s"
        timeInterval = 0;
    }
    
    func countDownTimer() {
        timeInterval = timeInterval + 1.0;
        updateTimeLabelWithNewTimeInterval()
    }
    
    func updateTimeLabelWithNewTimeInterval() {
        var remainingTime = timeInterval
        let minutes:Int = Int(remainingTime! / 60)
        var resultStr:String!
        
        // Minutes
        if(minutes != 0) {
            if(String(describing: minutes).characters.count < 2)
            {
                resultStr = String(0) + "\(minutes)"
            } else {
                resultStr = "\(minutes)"
            }
            remainingTime = remainingTime! -  TimeInterval(60 * minutes)
        }  else {
            resultStr = "00"
        }
        
        resultStr = resultStr + "m:"
        
        // Seconds

        if(String(describing: remainingTime).characters.count < 2) {
            resultStr = resultStr + String(0)
        }
        resultStr = resultStr + String.localizedStringWithFormat("%.f",remainingTime!)
        
        resultStr = resultStr + "s"
        
        timeLabel.text = resultStr
    }
}
