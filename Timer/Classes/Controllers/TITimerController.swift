//
//  TITimerViewController.swift
//  Timer
//
//  Created by David on 11/5/16.
//  Copyright © 2016 David. All rights reserved.
//

import UIKit

class TITimerViewController: UIViewController {
    
    @IBOutlet weak var contolPanelView: UIView!
    @IBOutlet weak var startTimerButton: TIRoundButton!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var timeLabelTopConstraint: NSLayoutConstraint!
    
    var timerInputView:TITimerInputView!
    var timer:Timer!
    var timeInterval:TimeInterval! = kDefaultTimeInterval

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.commonInit();
    }
    
    func commonInit() {
        // Setup transparent gradient
        let gradient = CAGradientLayer()
        gradient.frame = contolPanelView.bounds;
        
        gradient.colors = [UIColor.init(white: 0, alpha: 0.2).cgColor,UIColor.init(white: 0, alpha: 0.65).cgColor]
        gradient.locations = [0.0,1.0]
        gradient.startPoint = CGPoint(x:0.0, y:0.0)
        gradient.endPoint = CGPoint(x:0.5, y:1.0)
        contolPanelView.layer.insertSublayer(gradient, at: 0)
        
        startTimerButton.borderColor = UIColor(red:182.0/255.0, green:145.0/255.0, blue:14.0/255.0, alpha:1.0)
        
        timeLabelTopConstraint.constant = (view.frame.height - contolPanelView.frame.origin.y) / 2;
    }
    
    func initTimerInputView() {
        timerInputView = TITimerInputView.instanceFromNib()
        timerInputView.delegate = self
        view.addSubview(timerInputView)
        // Layout view
        timerInputView.translatesAutoresizingMaskIntoConstraints = false;
        let metrics = ["topMargin":  timeLabelTopConstraint.constant + timeLabel.frame.size.height / 3, "frameHeight": timeLabel.frame.size.height]
        let views = ["timerInputView": timerInputView]
        var allConstraints = [NSLayoutConstraint]()
        
        allConstraints += NSLayoutConstraint.constraints(
            withVisualFormat: "V:|-topMargin-[timerInputView(frameHeight)]",
            options: [],
            metrics: metrics,
            views: views)
        
        allConstraints += NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-15-[timerInputView]-15-|",
            options: [],
            metrics: nil,
            views: views)
        self.view.addConstraints(allConstraints)
    }
    
    @IBAction func setButtonDidTap(_ sender: UIButton) {
        if(timerInputView == nil) {
            initTimerInputView()
        }
        if(timer != nil) {
            timer.invalidate()
        }
        
        timeLabel.isHidden = true
        timerInputView.isHidden = false
        
        timerInputView.timerStartEndEditing()
    }
   
    
    @IBAction func resetButtonDidTap(_ sender: Any) {
        timerInputView.reset()
        timeInterval = kDefaultTimeInterval
        timer.invalidate()
    }
    
    @IBAction func startButtonDidTap(_ sender: Any) {
        if(timer == nil || timer.isValid == false) {
            timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(TITimerViewController.countDownTimer), userInfo: nil, repeats: true)
        }
    }
    
    @IBAction func handleTapGesture(_ sender: UITapGestureRecognizer) {
        if(timerInputView != nil) {
            timeLabel.isHidden = false
            timerInputView.isHidden = true
            self.view.endEditing(true)
        }
    }
    
    func countDownTimer() {
        if(timerInputView == nil) {
            initTimerInputView()
            timerInputView.isHidden = true
        }
        
        timeInterval = timeInterval - 1.0;
        let interval = timerInputView.initInterfaceWithTime(time: timeInterval)
        timerInputTimeDidChanged(hours: interval.0,minutes: interval.1,seconds: interval.2)
    }
}

extension TITimerViewController: TITimerInputViewDelegate {
    func timerInputTimeDidChanged(hours:String, minutes:String, seconds:String) {
        if (hours != "00") {
             timeLabel.text = "\(hours)h:\(minutes)m:\(seconds)s"
        } else {
             timeLabel.text = "\(minutes)m:\(seconds)s"
        }
        timeInterval = 0.0
        if let lHours = Double(hours) {
          timeInterval = lHours * 3600.0
        }
        if let lMinutes = Double(minutes) {
            timeInterval = timeInterval + lMinutes * 60.0
        }
        if let lSeconds = Double(seconds) {
            timeInterval = timeInterval + lSeconds
        }
    }
}

