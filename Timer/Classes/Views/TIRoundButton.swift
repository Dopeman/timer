//
//  TIRoundButton.swift
//  Timer
//
//  Created by David on 11/5/16.
//  Copyright © 2016 David. All rights reserved.
//

import UIKit

class TIRoundButton: UIButton {
    var borderColor:UIColor! = UIColor.white {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    func commonInit() {
        clipsToBounds = true
        layer.cornerRadius = self.frame.width / 2.0
        layer.borderColor = UIColor.white.cgColor
        layer.borderWidth = 1.0
    }
    
    open override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with : event)
        layer.borderColor = borderColor.withAlphaComponent(0.5).cgColor
    }
    
    open override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with : event)
        layer.borderColor = borderColor.cgColor
    }
}
