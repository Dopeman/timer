//
//  TITimerInputView.swift
//  Timer
//
//  Created by David on 11/5/16.
//  Copyright © 2016 David. All rights reserved.
//

import UIKit

protocol TITimerInputViewDelegate: class {
    func timerInputTimeDidChanged(hours:String, minutes:String, seconds:String)
}

let kViewsInnerMargin:CGFloat = 15.0
let kInputViewsCount:Int = 3
let kDefaultTimeInterval:TimeInterval = 300 // 5 min

class TITimerInputView: UIView,UITextFieldDelegate {
    
    @IBOutlet weak var inputViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var hoursTextField: UITextField!
    @IBOutlet weak var minutesTextField: UITextField!
    @IBOutlet weak var secondsTextField: UITextField!
    
    weak var delegate:TITimerInputViewDelegate?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    class func instanceFromNib() -> TITimerInputView {
        return  Bundle.main.loadNibNamed("TITimerInputView", owner: self, options: nil)?[0] as! TITimerInputView
    }
        
    override func awakeFromNib() {
        super.awakeFromNib()
        hoursTextField.delegate = self;
        minutesTextField.delegate = self;
        secondsTextField.delegate = self;
        
        initInterfaceWithTime(time: kDefaultTimeInterval);
    }
    
    override func layoutSubviews() {
        inputViewWidthConstraint.constant = (self.frame.size.width - 6 * kViewsInnerMargin) / CGFloat(kInputViewsCount)
    }
    
    func reset () {
        initInterfaceWithTime(time: kDefaultTimeInterval);
        delegate?.timerInputTimeDidChanged(hours: hoursTextField.text!,minutes: minutesTextField.text!,seconds: secondsTextField.text!)
    }
    
    @discardableResult func initInterfaceWithTime(time : TimeInterval) -> (String, String, String){
        var remainingTime = time
        let hours:Int = Int(time / 3600)
        if(hours != 0) {
            if(String(describing: hours).characters.count < 2)
            {
                hoursTextField.text = String(0) + "\(hours)"
            } else {
                hoursTextField.text = "\(hours)"
            }
            remainingTime = remainingTime - TimeInterval(3600 * hours)
        } else {
            hoursTextField.text = "00"
        }
        let minutes = Int(remainingTime / 60)
        if(minutes != 0) {
            if(String(describing: minutes).characters.count < 2)
            {
                minutesTextField.text = String(0) + "\(minutes)"
            } else {
                minutesTextField.text = "\(minutes)"
            }
            remainingTime = remainingTime -  TimeInterval(60 * minutes)
        } else {
            minutesTextField.text = "00"
        }
        
        if(remainingTime == 0) {
             secondsTextField.text = "00"
        } else if(String(describing: remainingTime).characters.count < 2)
        {
            secondsTextField.text = String(0) + String.localizedStringWithFormat("%.f",remainingTime)
        } else {
            secondsTextField.text =  String.localizedStringWithFormat("%.f",remainingTime)
        }
        return (hoursTextField.text!,minutesTextField.text!,secondsTextField.text!)
    }
    
    func timerStartEndEditing() {
        if(hoursTextField != nil) {
            if(isHidden == false) {
                hoursTextField.becomeFirstResponder()
            } else {
                self.endEditing(true)
            }
        }
    }
    

    // MARK: UITextField data source
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        let userEnteredString = textField.text
        let resultStr = (userEnteredString! as NSString).replacingCharacters(in: range, with: string) as NSString
        if(resultStr.length > 2) {
            if(hoursTextField == textField) {
                minutesTextField.becomeFirstResponder()
            }
            if(minutesTextField == textField) {
                secondsTextField.becomeFirstResponder()
            }
            return false
        }
        if(hoursTextField == textField) {
             delegate?.timerInputTimeDidChanged(hours: resultStr as String,minutes: minutesTextField.text!,seconds: secondsTextField.text!)
        }
        if(minutesTextField == textField) {
            delegate?.timerInputTimeDidChanged(hours:hoursTextField.text!,minutes:resultStr as String!,seconds: secondsTextField.text!)
        }
        else {
            delegate?.timerInputTimeDidChanged(hours:hoursTextField.text!,minutes:minutesTextField.text!,seconds: resultStr as String)
        }
       
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        textField.text = ""
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        if(textField.text?.characters.count == 0) {
            textField.text = "00"
        }
        if(minutesTextField == textField) {
            let minutes = Int(textField.text!)
            if(minutes! > 60) {
                textField.text = "60"
            }
        }
        if(secondsTextField == textField) {
            let seconds = Int(textField.text!)
            if(seconds! > 60) {
                textField.text = "60"
            }
        }
        delegate?.timerInputTimeDidChanged(hours: hoursTextField.text!,minutes: minutesTextField.text!,seconds: secondsTextField.text!)
    }
}
